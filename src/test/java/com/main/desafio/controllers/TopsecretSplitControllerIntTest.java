package com.main.desafio.controllers;

import com.main.desafio.entities.DataCommunication;
import com.main.desafio.entities.Point;
import com.main.desafio.services.DataCommunicationService;
import com.main.desafio.services.TopSecretService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TopsecretSplitController.class})
@WebAppConfiguration
@EnableWebMvc
@ActiveProfiles("logging-test")
public class TopsecretSplitControllerIntTest {
    private MockMvc mockMvc;

    @MockBean
    private TopSecretService topSecretService;

    @MockBean
    private DataCommunicationService dataCommunicationService;


    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    /**
     * TEST GET /topsecret_split
     */

    // Informacion de los tres satelites correcta
    @Test
    public void getDataSuccessful()throws  Exception {
        DataCommunication dataKenobi = new DataCommunication(1L,"kenobi",412.31058f, new String[] {"esto","es","un","mensaje","valido"});
        DataCommunication dataSkywalker = new DataCommunication(1L,"skywalker",200.0f, new String[] {"esto","es","un","mensaje","valido"});
        DataCommunication dataSato = new DataCommunication(1L,"sato",632.4555f, new String[] {"esto","es","un","mensaje","valido"});
        List<DataCommunication> list= new ArrayList<>();
        list.add(dataKenobi);
        list.add(dataSkywalker);
        list.add(dataSato);
        when(topSecretService.GetMessage(any(String[].class),any(String[].class),any(String[].class)))
                .thenReturn("esto es un mensaje valido");
        when(topSecretService.GetLocation(any(Float.class),any(Float.class),any(Float.class)))
                .thenReturn(new Point(-100,-100));
        when(dataCommunicationService.findAll())
                .thenReturn(list);
        mockMvc.perform(MockMvcRequestBuilders.get("/topsecret_split")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("{'position':{'x':-100.0,'y':-100.0},'message':'esto es un mensaje valido'}"));
    }

    // Sin informacion del satelite kenobi
    @Test
    public void getDataErrorMissingData()throws  Exception {
        DataCommunication dataSkywalker = new DataCommunication(1L,"skywalker",200.0f, new String[] {"esto","es","un","mensaje","valido"});
        DataCommunication dataSato = new DataCommunication(1L,"sato",632.4555f, new String[] {"esto","es","un","mensaje","valido"});
        List<DataCommunication> list= new ArrayList<>();
        list.add(dataSkywalker);
        list.add(dataSato);
        when(dataCommunicationService.findAll())
                .thenReturn(list);
        mockMvc.perform(MockMvcRequestBuilders.get("/topsecret_split")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    // Sin informacion del ningun satelite
    @Test
    public void getDataErrorEmptyData()throws  Exception {
        List<DataCommunication> list= new ArrayList<>();
        when(dataCommunicationService.findAll())
                .thenReturn(list);
        mockMvc.perform(MockMvcRequestBuilders.get("/topsecret_split")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    // Ubicacion invalida
    @Test
    public void getDataErrorInvalidLocation()throws  Exception {
        DataCommunication dataKenobi = new DataCommunication(1L,"kenobi",412.31058f, new String[] {"esto","es","un","mensaje","valido"});
        DataCommunication dataSkywalker = new DataCommunication(1L,"skywalker",-100.0f, new String[] {"esto","es","un","mensaje","valido"});
        DataCommunication dataSato = new DataCommunication(1L,"sato",632.4555f, new String[] {"esto","es","un","mensaje","valido"});
        List<DataCommunication> list= new ArrayList<>();
        list.add(dataKenobi);
        list.add(dataSkywalker);
        list.add(dataSato);
        when(dataCommunicationService.findAll())
                .thenReturn(list);
        when(topSecretService.GetMessage(any(String[].class),any(String[].class),any(String[].class)))
                .thenReturn("esto es un mensaje valido");
        when(topSecretService.GetLocation(any(Float.class),any(Float.class),any(Float.class)))
                .thenReturn(null);

        mockMvc.perform(MockMvcRequestBuilders.get("/topsecret_split")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    // Mensaje invalido
    @Test
    public void getDataErrorInvalidMessage()throws  Exception {
        DataCommunication dataKenobi = new DataCommunication(1L,"kenobi",412.31058f, new String[] {"esto","NO","un","mensaje","valido"});
        DataCommunication dataSkywalker = new DataCommunication(1L,"skywalker",200.0f, new String[] {"esto","es","un","mensaje","valido"});
        DataCommunication dataSato = new DataCommunication(1L,"sato",632.4555f, new String[] {"esto","es","un","mensaje","valido"});
        List<DataCommunication> list= new ArrayList<>();
        list.add(dataKenobi);
        list.add(dataSkywalker);
        list.add(dataSato);
        when(dataCommunicationService.findAll())
                .thenReturn(list);
        when(topSecretService.GetMessage(any(String[].class),any(String[].class),any(String[].class)))
                .thenReturn(null);
        when(topSecretService.GetLocation(any(Float.class),any(Float.class),any(Float.class)))
                .thenReturn(new Point(-100,-100));

        mockMvc.perform(MockMvcRequestBuilders.get("/topsecret_split")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    /**
     * TEST POST /topsecret_split/{satelliteName}
     */

    // POST /topsecret_split/kenobi
    @Test
    public void receiveDataSuccessful() throws  Exception{
        String bodyRequestCommunicationData = "{\"distance\":412.3105626,\"message\":[\"esto\",\"es\",\"un\",\"mensaje\",\"valido\"]}";
        DataCommunication dataKenobi = new DataCommunication(1L,"kenobi",412.3105626f, new String[] {"esto","es","un","mensaje","valido"});
        when(dataCommunicationService.findByName("kenobi"))
                .thenReturn(Optional.of(dataKenobi));
        when(dataCommunicationService.save(any(DataCommunication.class)))
                .thenReturn(dataKenobi);
        mockMvc.perform(MockMvcRequestBuilders.post("/topsecret_split/kenobi")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyRequestCommunicationData)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    // POST /topsecret_split/kenobi multiple call
    @Test
    public void receiveDataSuccessfulIdempotency() throws  Exception{
        String bodyRequestCommunicationData = "{\"distance\":412.3105626,\"message\":[\"esto\",\"es\",\"un\",\"mensaje\",\"valido\"]}";
        DataCommunication dataKenobi = new DataCommunication(1L,"kenobi",412.3105626f, new String[] {"esto","es","un","mensaje","valido"});
        when(dataCommunicationService.findByName("kenobi"))
                .thenReturn(Optional.of(dataKenobi));
        when(dataCommunicationService.save(any(DataCommunication.class)))
                .thenReturn(dataKenobi);
        mockMvc.perform(MockMvcRequestBuilders.post("/topsecret_split/kenobi")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyRequestCommunicationData)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mockMvc.perform(MockMvcRequestBuilders.post("/topsecret_split/kenobi")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyRequestCommunicationData)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    // POST /topsecret_split/invalid
    @Test
    public void receiveDataErrorInvalidSatellite() throws  Exception{
        String bodyRequestCommunicationData = "{\"distance\":412.3105626,\"message\":[\"esto\",\"es\",\"un\",\"mensaje\",\"valido\"]}";
        mockMvc.perform(MockMvcRequestBuilders.post("/topsecret_split/invalid")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyRequestCommunicationData)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    // POST /topsecret_split/kenobi (invalid distance)
    @Test
    public void receiveDataErrorInvalidDistance() throws  Exception{
        String bodyRequestCommunicationData = "{\"distance\":-100,\"message\":[\"esto\",\"es\",\"un\",\"mensaje\",\"valido\"]}";
        mockMvc.perform(MockMvcRequestBuilders.post("/topsecret_split/kenobi")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyRequestCommunicationData)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    // POST /topsecret_split/kenobi (invalid message)
    @Test
    public void receiveDataErrorInvalidMessage() throws  Exception{
        String bodyRequestCommunicationData = "{\"distance\":-100,\"message\":null}";
        mockMvc.perform(MockMvcRequestBuilders.post("/topsecret_split/kenobi")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyRequestCommunicationData)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    // POST /topsecret_split/kenobi empty body
    @Test
    public void receiveDataErrorEmptyBody() throws  Exception{
        String bodyRequestCommunicationData = "{}";
        mockMvc.perform(MockMvcRequestBuilders.post("/topsecret_split/kenobi")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyRequestCommunicationData)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

}