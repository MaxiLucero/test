package com.main.desafio.controllers;


import com.main.desafio.entities.Point;
import com.main.desafio.services.TopSecretService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TopsecretController.class})
@WebAppConfiguration
@EnableWebMvc
@ActiveProfiles("logging-test")
public class TopsecretControllerIntTest {
    private MockMvc mockMvc;

    @MockBean
    private TopSecretService topSecretService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void receiveDataSuccessful() throws  Exception{
        String bodyRequest = "{\"satellites\":[{\"name\":\"kenobi\",\"distance\":412.3105626,\"message\":[\"\",\"\",\"ESTO\",\"es\",\"un\",\"\",\"valido\"]},{\"name\":\"skywalker\",\"distance\":200,\"message\":[\"\",\"\",\"es\",\"\",\"\",\"\"]},{\"name\":\"sato\",\"distance\":632.455532,\"message\":[\"\",\"esto\",\"\",\"\",\"mejsaje\",\"\"]}]}";
        String expectedBodyRsponse = "{\"position\":{\"x\":-100,\"y\":-100},\"message\":\"esto es un mensaje valido\"}";
        when(topSecretService.GetMessage(any(String[].class),any(String[].class),any(String[].class)))
                .thenReturn("esto es un mensaje valido");
        when(topSecretService.GetLocation(any(Float.class),any(Float.class),any(Float.class)))
                .thenReturn(new Point(-100,-100));
        mockMvc.perform(MockMvcRequestBuilders.post("/topsecret")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyRequest)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(expectedBodyRsponse));
    }

    // POST /topsecret distancia negativa a un satellite
    @Test
    public void receiveDataErrorInvalidDist() throws  Exception{
        String bodyRequest = "{\"satellites\":[{\"name\":\"kenobi\",\"distance\":-412.3105626,\"message\":[\"esto\",\"es\",\"un\",\"mensaje\",\"valido\"]},{\"name\":\"skywalker\",\"distance\":200,\"message\":[\"esto\",\"es\",\"un\",\"mensaje\",\"valido\"]},{\"name\":\"sato\",\"distance\":632.455532,\"message\":[\"esto\",\"es\",\"un\",\"mejsaje\",\"valido\"]}]}";
        mockMvc.perform(MockMvcRequestBuilders.post("/topsecret")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyRequest)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    // POST /topsecret mensaje invalido en satellite skywalker
    @Test
    public void receiveDataErrorInvalidMsg1() throws  Exception{
        String bodyRequest = "{\"satellites\":[{\"name\":\"kenobi\",\"distance\":412.3105626,\"message\":[\"esto\",\"es\",\"un\",\"\",\"valido\"]},{\"name\":\"skywalker\",\"distance\":200,\"message\":[\"es\",\"\",\"\",\"valido\"]},{\"name\":\"sato\",\"distance\":632.455532,\"message\":[\"esto\",\"\",\"\",\"mejsaje\",\"\"]}]}";
        mockMvc.perform(MockMvcRequestBuilders.post("/topsecret")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyRequest)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    // POST /topsecret sin mensaje en satellite kenobi
    @Test
    public void reciveDataErrorNullMsg() throws  Exception{
        String bodyRequest = "{\"satellites\":[{\"name\":\"kenobi\",\"distance\":412.3105626},{\"name\":\"skywalker\",\"distance\":200,\"message\":[\"esto\",\"es\",\"un\",\"mensaje\",\"valido\"]},{\"name\":\"sato\",\"distance\":632.455532,\"message\":[\"esto\",\"es\",\"un\",\"mejsaje\",\"valido\"]}]}";
        mockMvc.perform(MockMvcRequestBuilders.post("/topsecret")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyRequest)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    // POST /topsecret sin objeto satellites en el body
    @Test
    public void reciveDataMissingAll() throws  Exception{
        String bodyRequest = "{}";
        mockMvc.perform(MockMvcRequestBuilders.post("/topsecret")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyRequest)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    // POST /topsecret sin informacion del satelite SATO
    @Test
    public void reciveDataMissingPartial() throws  Exception{
        String bodyRequest = "{\"satellites\":[{\"name\":\"kenobi\",\"distance\":412.3105626,\"message\":[\"esto\",\"es\",\"un\",\"\",\"valido\"]},{\"name\":\"skywalker\",\"distance\":200.0000001,\"message\":[\"\",\"es\",\"\",\"mensaje\",\"\"]}]}";
        mockMvc.perform(MockMvcRequestBuilders.post("/topsecret")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyRequest)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }
}