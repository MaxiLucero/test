package com.main.desafio.services;

import com.main.desafio.entities.Point;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(MockitoJUnitRunner.class)
public class TopSecretServiceTest {
    final String expectedMsg = "esto es un mensaje secreto";

    @InjectMocks
    TopSecretService topSecretService;

    /**
     *  UNIT TEST GetLocation
     *      input= float distanceP1, float distanceP2, float DistanceP3
     *      output
     *          success = Point(x,y)
     *          error = null
     */

    @Test
    // Distance to Point( -200, 200)
    public void getLocationSuccessP1() {
        float d1 =500 ,d2 = (float)(300*Math.sqrt(2)),d3=(float)(500*Math.sqrt(2));
        Point expectedPoint = new Point(-200,200);
        Point resultPoint = topSecretService.GetLocation(d1, d2, d3);
        assertEquals(expectedPoint.getX(),resultPoint.getX(),topSecretService.ACCEPTED_DELTA);
        assertEquals(expectedPoint.getY(),resultPoint.getY(),topSecretService.ACCEPTED_DELTA);
    }

    @Test
    // Distance to Point Point( -100, -100)
    public void getLocationSuccessP2() {
        float d1 =(float)(100*Math.sqrt(17)) ,d2 = 200 ,d3=(float)(200*Math.sqrt(10));
        Point expectedPoint = new Point(-100,-100);
        Point resultPoint = topSecretService.GetLocation(d1, d2, d3);
        assertEquals(expectedPoint.getX(),resultPoint.getX(),topSecretService.ACCEPTED_DELTA);
        assertEquals(expectedPoint.getY(),resultPoint.getY(),topSecretService.ACCEPTED_DELTA);
    }

    @Test
    // Distance to Point Punto (0 ,0)
    public void getLocationSuccessP3() {
         float d1 =(float)(100*Math.sqrt(29)) ,d2 = (float)(100*Math.sqrt(2)) ,d3=(float)(100*Math.sqrt(26));
        Point expectedPoint = new Point(0,0);
        Point resultPoint = topSecretService.GetLocation(d1, d2, d3);
        assertEquals(expectedPoint.getX(),resultPoint.getX(),topSecretService.ACCEPTED_DELTA);
        assertEquals(expectedPoint.getY(),resultPoint.getY(),topSecretService.ACCEPTED_DELTA);
    }

    @Test
    // Distance to satelite Kenobi
    public void getLocationSuccessP4() {
        float d1 =0 ,d2 = (float)(100*Math.sqrt(37)) ,d3=(float)(100*Math.sqrt(109));
        Point expectedPoint = new Point(-500,-200);
        Point resultPoint = topSecretService.GetLocation(d1, d2, d3);
        assertEquals(expectedPoint.getX(),resultPoint.getX(),topSecretService.ACCEPTED_DELTA);
        assertEquals(expectedPoint.getY(),resultPoint.getY(),topSecretService.ACCEPTED_DELTA);
    }

    @Test
    public void getLocationErrorDistanceNegativeInvalid() {
        float d1 =(float)(100*Math.sqrt(17)) ,d2 = -300 ,d3=(float)(200*Math.sqrt(10));
        Point resultPoint = topSecretService.GetLocation(d1, d2, d3);
        assertNull(resultPoint);
    }

    @Test
    public void getLocationErrorDistanceValueInvalid() {
        float d1 =(float)(100*Math.sqrt(17)) ,d2 = 500 ,d3=(float)(200*Math.sqrt(10));
        Point resultPoint = topSecretService.GetLocation(d1, d2, d3);
        assertNull(resultPoint);
    }

    @Test
    public void getLocationErrorDistanceZero() {
        float d1 =0 ,d2 = 0 ,d3=0;
        Point resultPoint = topSecretService.GetLocation(d1, d2, d3);
        assertNull(resultPoint);
    }

    @Test
    // Distance to Point(500000,500000)
    public void getLocationErrorBorder() {
        float d1 =(float)(100*Math.sqrt(50070029)) ,d2 = (float)(1300*Math.sqrt(295858)) ,d3=(float)(100*Math.sqrt(49940026));
        Point expectedPoint = new Point(500000,500000);
        Point resultPoint = topSecretService.GetLocation(d1, d2, d3);
        assertNull(resultPoint);
    }

    /**
     *  UNIT TESTS GetMessage
     *      input= String[] distanceP1, String[] distanceP2, String[] DistanceP3
     *      output
     *          success = String
     *          error = null
     */

    @Test
    public void getMessagesSuccessful() {
        String resultMsg = topSecretService.GetMessage(
                new String[]{"esto", "es", "un", ""       , "secreto"},
                new String[]{    "", "es",   "", "mensaje", ""       },
                new String[]{"esto",   "", "un", ""       , "secreto"});
        assertEquals(expectedMsg, resultMsg);
    }

    @Test
    public void getMessagesSuccessfulUppercase() {
        String resultMsg = topSecretService.GetMessage(
                new String[]{    "", "ES",   "",        "","SeCreTo"},
                new String[]{"ESTO",   "", "un", "MENSAJE",""       },
                new String[]{    "", "es",   "",        "","secreto"});
        assertEquals(expectedMsg, resultMsg);
    }

    @Test
    public void getMessagesSuccessfulOneEmpty() {
        String resultMsg = topSecretService.GetMessage(
                new String[]{    "",   "",   "", ""       , ""       },
                new String[]{    "", "es", "un", "mensaje", ""       },
                new String[]{"esto",   "", "un", ""       , "secreto"});
        assertEquals(expectedMsg, resultMsg);
    }

    @Test
    public void getMessagesSuccessfulTwoEmpty() {
        String resultMsg = topSecretService.GetMessage(
                new String[]{    "",   "",   "", ""       , ""       },
                new String[]{    "",   "",   "", ""       , ""       },
                new String[]{"esto", "es", "un", "mensaje", "secreto"});
        assertEquals(expectedMsg, resultMsg);
    }

    @Test
    // 1 messages shifted
    public void getMessagesSuccessShifted1() {
        String resultMsg = topSecretService.GetMessage(
                new String[]{            "", "es",   "",        "","secreto"},
                new String[]{"", "", "esto",   "", "un", "mensaje",""       },
                new String[]{            "",   "",   "", "mensaje","secreto"});
        assertEquals(expectedMsg, resultMsg);
    }

    @Test
    // 2 messages shifted
    public void getMessagesSuccessShifted2() {
        String resultMsg = topSecretService.GetMessage(
                new String[]{    "",     "", "es",   "",        "","secreto"},
                new String[]{"", "", "esto",   "", "un", "mensaje",""       },
                new String[]{            "",   "",   "",        "","secreto"});
        assertEquals(expectedMsg, resultMsg);
    }

    @Test
    // All messages shifted
    public void getMessagesSuccessShifted3() {
        String resultMsg = topSecretService.GetMessage(
                new String[]{    "",     "", "es",   "",        "","secreto"},
                new String[]{"", "", "esto",   "", "un", "mensaje",""       },
                new String[]{    "",     "",   "",   "",        "","secreto"});
        assertEquals(expectedMsg, resultMsg);
    }

    @Test
    public void getMessagesErrorAllEmpty() {
        String resultMsg = topSecretService.GetMessage(
                new String[]{    "",   "",   "", ""       , ""       },
                new String[]{    "",   "",   "", ""       , ""       },
                new String[]{    "",   "",   "", ""       , ""       });;
        assertNull(resultMsg);
    }

    @Test
    public void getMessagesErrorOneNull() {
        String resultMsg = topSecretService.GetMessage( new String[]{"", "", "", ""}, new String[]{"esto", "es", "un", "mensaje"},null);
        assertNull(resultMsg);
    }

    @Test
    public void getMessagesErrorDifferentSize() {
        String resultMsg = topSecretService.GetMessage(
                new String[]{    "",   "", "un", ""       , "secreto"},
                new String[]{"esto", "es",   "", "mensaje", ""       },
                new String[]{    "", "un", ""       , "secreto"});
        assertNull(resultMsg);
    }

}