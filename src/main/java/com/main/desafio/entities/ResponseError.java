package com.main.desafio.entities;

public class ResponseError {
    public static final String SATELLITE_NOT_RECOGNIZED = "No se reconoce el satelite.";
    public static final String MISSING_INVALID_SATELLITE = "Informacion de satelite invalida o faltante.";
    public static final String INDETERMINATE_POSITION_MESSAGE = "No se pudo determinar la posicion/mensaje.";

    private int error;
    private String message;
  
    public ResponseError(int error, String msg) {
        this.error = error;
        this.message = msg;
    }

    public void ResponseError() {
        this.error = 500;
        this.message = "Internal Error";
    }

    public int getError() {
        return this.error;
    }

    public void setError(int httpStatus) {
        this.error = httpStatus;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
