package com.main.desafio.entities;

import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name = "data_communications")
public class DataCommunication {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="name", nullable = false, unique = true)
    private String name;

    @Column(name="distance", nullable = false)
    private float distance = -1f;

    @Column(name="message", nullable = false)
    private String[] message;

    public DataCommunication(Long id, String name, float distance, String[] message) {
        this.id = id;
        this.name = name;
        this.distance = distance;
        this.message = message;
    }

    public DataCommunication(String name, float distance, String [] message){
        this.name = name;
        this.distance = distance;
        this.message = message;
    }
    public DataCommunication(String name){
        this.name = name;
    }

    public DataCommunication(){
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public String[] getMessage() {
        return message;
    }

    public void setMessage(String[] message)  {
        this.message = message;
    }

    public boolean validate(){
        if(message == null || name == null || name.isEmpty() || distance < 0.0f )
            return false;
        else
            return true;
    }
}
