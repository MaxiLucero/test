package com.main.desafio.services;

import com.main.desafio.entities.Point;
import org.springframework.stereotype.Service;

@Service
public class TopSecretService {
    public final float KENOBI_X_LOCATION = -500.0f;
    public final float KENOBI_Y_LOCATION = -200.0f;
    public final float SKYWALKER_X_LOCATION = 100.0f;
    public final float SKYWALKER_Y_LOCATION = -100.0f;
    public final float SATO_X_LOCATION = 500.0f;
    public final float SATO_Y_LOCATION = 100.0f;
    public final float ACCEPTED_DELTA = 0.2f;

    public Point GetLocation(float r1, float r2, float r3) {
        Point emisorLocation = new Point();
        // Posicion satelite 1
        float x1 = KENOBI_X_LOCATION;
        float y1 = KENOBI_Y_LOCATION;
        // Posicion satelite 2
        float x2 = SKYWALKER_X_LOCATION;
        float y2 = SKYWALKER_Y_LOCATION;
        // Posicion satelite 3
        float x3 = SATO_X_LOCATION;
        float y3 = SATO_Y_LOCATION;

        // Trilateracion
        float valueA = (-2 * x1 + 2 * x2);
        float valueB = (-2 * y1 + 2 * y2);
        float valueC = (float)(Math.pow(r1, 2) - Math.pow(r2, 2) -Math.pow(x1, 2) +Math.pow(x2, 2) - Math.pow(y1, 2) + Math.pow(y2, 2));
        float valueD = (-2 * x2 + 2 * x3);
        float valueE = (-2 * y2 + 2 * y3);
        float valueF = (float)(Math.pow(r2, 2) - Math.pow(r3, 2) -Math.pow(x2, 2) +Math.pow(x3, 2) - Math.pow(y2, 2) + Math.pow(y3, 2));
        float x ,y;
        try {
            x = ( valueC * valueE - valueF * valueB ) / (valueE * valueA - valueB * valueD);
            y = ( valueC * valueD - valueA * valueF ) / (valueB * valueD - valueA * valueE);
        } catch (ArithmeticException e) {
            System.out.println("Error al calular formula trilateracion...");
            return null;
        }
        emisorLocation.setX(x);
        emisorLocation.setY(y);
        if(validateLocation(r1,r2,r3,emisorLocation))
            return  emisorLocation;
        else
            return null;
    }

    public boolean validateLocation(float r1, float r2, float r3, Point emisorLocation) {
        if(emisorLocation !=null){
            /** 
             *  SISTEMA DE ECUACION DE DISTANCIA 
             *  (x-x1)**2 = (y-y1)**2 = r1**2
             *  (x-x2)**2 = (y-y2)**2 = r2**2
             *  (x-x3)**2 = (y-y3)**2 = r3**2 
             */
            float valueA1 = (float) (Math.pow((emisorLocation.getX() - KENOBI_X_LOCATION), 2) +  
                            Math.pow((emisorLocation.getY() - KENOBI_Y_LOCATION),2));
            float valueA2 = (float) Math.pow(r1,2);
            float valueB1 = (float) (Math.pow((emisorLocation.getX() - SKYWALKER_X_LOCATION), 2) +  
                            Math.pow((emisorLocation.getY() - SKYWALKER_Y_LOCATION),2));
            float valueB2 = (float) Math.pow(r2,2);
            float valueC1 = (float) (Math.pow((emisorLocation.getX() - SATO_X_LOCATION), 2) +  
                            Math.pow((emisorLocation.getY() - SATO_Y_LOCATION),2));
            float valueC2 = (float) Math.pow(r3,2);
            float dif1 = valueA1 - valueA2;
            float dif2 = valueB1 - valueB2;
            float dif3 = valueC1 - valueC2;
            // Calculo valor absoluto de las diferencias
            dif1 = dif1 > 0 ? dif1 : -dif1;
            dif2 = dif2 > 0 ? dif2 : -dif2;
            dif3 = dif3 > 0 ? dif3 : -dif3;
            //Comparo con el valor epsilon de error aceptado
            if(dif1 < ACCEPTED_DELTA && dif2 < ACCEPTED_DELTA && dif3 < ACCEPTED_DELTA)
                return true;
            else
                return false;
        }
        else
            return  false;
    }

    public String GetMessage(String[] msgA, String[] msgB, String[] msgC ){
        String [] finalMsg;
        boolean flagErrorMsg = false;
        boolean flagDesfasaje = true; // Flag para descartar desfasaje comun a los tres mensajes

        if(msgA == null || msgB == null || msgC == null)
            return null;

        int aSize = msgA.length; // Msg Kenobi
        int bSize = msgB.length; // Msg Skywalker
        int cSize = msgC.length; // Msg Sato

        // Determino cual es el mensaje con menos desfasaje y los dejo en msgA
        if(!(aSize <= bSize && aSize <= cSize)){ // A no es el msg con menos desfasaje
            if(bSize <= aSize && bSize <= cSize){ // B es el msg con menos desfasaje
                String[] temp = msgA;
                msgA = msgB;
                msgB = temp;
            }
            else{ // C es el msg con menos desfasaje
                String[] temp = msgA;
                msgA = msgC;
                msgC = temp;
            }
        }

        int startA,startB,startC;
        finalMsg = new String[msgA.length];

        startA = 0;
        startB = (msgB.length - msgA.length); // Descarto desfasaje de B
        startC = (msgC.length - msgA.length); // Descarto desfasaje de C

        //Analiso si los strings descartados corresponden a desfasajes
        for (int i=0; i< startB; i++){
            if (!msgB[i].equals(""))
                return null;
        }
        for (int i=0; i< startC; i++){
            if (!msgC[i].equals(""))
                return null;
        }

        for(int index = 0; ( index < msgA.length && flagErrorMsg == false ) ;index++){
            String strA = msgA[startA + index].toLowerCase();
            String strB = msgB[startB + index].toLowerCase();
            String strC = msgC[startC + index].toLowerCase();
            String res;
            if(flagDesfasaje && strA.isEmpty() && strB.isEmpty() && strC.isEmpty())
                finalMsg[index] = "";
            else{
                flagDesfasaje = false;
                if(!strA.isEmpty()){
                    res = compareStr(strA,strB);
                    if(res != null) // Comparacion exitosa entre A y B
                        res = compareStr(strA,strC);
                }
                else{ // A es "" en index, solo comparo B y C
                    if(!strB.isEmpty())
                        res = compareStr(strB,strC);
                    else{ // A y B son "" en la posicion index
                        if(strC.isEmpty()) // Los 3 msg en la posicion index son ""
                            res = null;
                        else
                            res = strC; 
                    }
                }
                if(res != null)
                    finalMsg[index] = res;
                else
                    flagErrorMsg = true;
            }
        }

        if(flagErrorMsg)
            return null;
        else{
            String msg = String.join(" ",finalMsg);
            msg = msg.trim();
            if( !msg.isEmpty())
                return msg;
            else
                return null;
        }
    }

    private String compareStr(String a, String b){
        // Compara el String a (no empty) con el String b
        if(b.isEmpty()|| a.equals(b))
            return a;
        else
            return null;
    }
    
}
