package com.main.desafio.services;

import com.main.desafio.entities.DataCommunication;
import com.main.desafio.repository.DataCommunicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class DataCommunicationService {

    @Autowired
    DataCommunicationRepository dataCommunicationRepository;

    public Optional<DataCommunication> findByName(String name) {
        return  dataCommunicationRepository.findByName(name);
    }

    public List<DataCommunication> findAll(){
        return dataCommunicationRepository.findAll();
    }
   
    public DataCommunication save(DataCommunication s){
        return dataCommunicationRepository.save(s);
    }

    public void deleteAll(){
        dataCommunicationRepository.deleteAll();
        return;
    }

}
