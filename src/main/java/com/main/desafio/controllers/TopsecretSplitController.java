package com.main.desafio.controllers;

import com.main.desafio.entities.DataCommunication;
import com.main.desafio.entities.Point;
import com.main.desafio.entities.ResponseError;
import com.main.desafio.services.DataCommunicationService;
import com.main.desafio.services.TopSecretService;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping ("/topsecret_split")
public class TopsecretSplitController {
    @Autowired
    DataCommunicationService dataCommunicationService;

    @Autowired
    TopSecretService topSecretService;

    /**
     * GET /topsecret_split
     * Permite recuperar la posicion y mensaje de la nave
     * RESPONSE BODY: JSON { "position": {"x": pos_x , "y": pos_y }, "message": "mensaje enviado por la nave"}
     * RESPONSE CODE: 200 (Exito) / 404 (Error)
     */
    @GetMapping()
    @ResponseBody
    public ResponseEntity<Object> getData() {
        boolean flagError = false;
        // Al recuperar la informacion, se sigue conservando la informacion de distancia a los satelites y mensajes
        List<DataCommunication> list = dataCommunicationService.findAll();
        System.out.println("list = " + list.toString());
        // validacion de los datos recibidos
        if (list.size() == 3 ){
            DataCommunication aData = null ,bData = null,cData = null;
            Point position = null;
            String message = null;
            for (Iterator<DataCommunication> iter = list.iterator(); iter.hasNext();){
                DataCommunication s = iter.next();
                DataCommunication data = new DataCommunication(s.getName(),s.getDistance(),s.getMessage());
                if(!data.validate())
                    flagError = true;
                else{
                    switch(data.getName()){
                        case "kenobi": 
                            aData = new DataCommunication(data.getName(),data.getDistance(),data.getMessage());
                            break;
                        case "skywalker":
                            bData = new DataCommunication(data.getName(),data.getDistance(),data.getMessage());
                            break;
                        case "sato": 
                            cData = new DataCommunication(data.getName(),data.getDistance(),data.getMessage());
                            break;
                        default:{
                            return new ResponseEntity<Object>(new ResponseError(404, ResponseError.SATELLITE_NOT_RECOGNIZED), HttpStatus.NOT_FOUND);
                        }
                    }  
                }    
            }
            if(flagError || aData == null || bData == null || cData == null) // Llego informacion de algun satelite no reconocido
                return new ResponseEntity<Object>(new ResponseError(404, ResponseError.MISSING_INVALID_SATELLITE), HttpStatus.NOT_FOUND);

            position = topSecretService.GetLocation(aData.getDistance(),bData.getDistance(),cData.getDistance());
            message = topSecretService.GetMessage(aData.getMessage(),bData.getMessage(),cData.getMessage());
            if(message != null && position != null){
                JSONObject res = new JSONObject();
                res.put("position", position);
                res.put("message", message);
                return new ResponseEntity<Object>(res, HttpStatus.OK);  
            }
            else
                return new ResponseEntity<Object>(new ResponseError(404, ResponseError.INDETERMINATE_POSITION_MESSAGE), HttpStatus.NOT_FOUND);
        } else 
            return new ResponseEntity<Object>(new ResponseError(404, ResponseError.MISSING_INVALID_SATELLITE), HttpStatus.NOT_FOUND);
    }

    /**
     * POST /topsecret_split/{satelliteName}
     * Permite almacenar la informacion de distancia y mensaje del satelite indicado por parametro
     * REQUEST BODY: JSON {"distance":float_value, "message":["esto","enviado","por","la","nave"]}
     * RESPONSE CODE: 200 (Exito) / 404 (Error)
     */
    @PostMapping("/{satelliteName}")
    @ResponseBody
    public ResponseEntity<Object> receiveData(@PathVariable String satelliteName, @RequestBody  DataCommunication data) {
        if(satelliteName.equals("kenobi") || satelliteName.equals("skywalker") || satelliteName.equals("sato")){
            data.setName(satelliteName);
            if (data.validate()){
                Optional<DataCommunication> optionalData = dataCommunicationService.findByName(satelliteName);
                DataCommunication d;
                if( optionalData.isPresent())
                    d = optionalData.get();
                else
                    d = new DataCommunication(data.getName());
                d.setDistance(data.getDistance());
                d.setMessage(data.getMessage());
                dataCommunicationService.save(d);
                return ResponseEntity.ok(d);
            }
            else
                return new ResponseEntity(new ResponseError(404, ResponseError.MISSING_INVALID_SATELLITE), HttpStatus.NOT_FOUND);
        }
        else
            return new ResponseEntity(new ResponseError(404, ResponseError.SATELLITE_NOT_RECOGNIZED), HttpStatus.NOT_FOUND);

    }


}