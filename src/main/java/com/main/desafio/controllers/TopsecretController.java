package com.main.desafio.controllers;

import com.main.desafio.entities.DataCommunication;
import com.main.desafio.entities.Point;
import com.main.desafio.entities.ResponseError;
import com.main.desafio.services.TopSecretService;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping ("/topsecret")
public class TopsecretController {
    @Autowired
    TopSecretService topSecretService;

    /**
     * POST /topsecret
     * Permite recuperar la infomacion almacenada de todos los satelites
     * REQUEST BODY: JSON con listado de satelites(float dsitancia, string[] mensaje)
     * RESPONSE BODY: JSON { "position": {"x": pos_x , "y": pos_y }, "message": "mensaje enviado por la nave"}
     * RESPONSE CODE: 200 (Exito) / 404 (Error)
     */
    @PostMapping()
    @ResponseBody
    public Object receiveData(@RequestBody  Map<String,List<DataCommunication>> body) {
        boolean flagError = false;
        List<DataCommunication> satellites = null;
        if(body.containsKey("satellites")){
            satellites = body.get("satellites");
            // validacion de los datos recibidos
            if (satellites.size() == 3 ){
                for(int j=0; j< satellites.size(); j++){
                    if(!satellites.get(j).validate()) // Los datos del satelite no son validos
                        flagError = true;
                }
            }
            else // No se enviaron los datos de tres satelites
                flagError = true;
        }
        else // No llego el elemento satellites
            flagError = true;

        if(!flagError){
            DataCommunication aData = null ,bData = null,cData = null;
            Point position = null;
            String message = null;
            for (Iterator<DataCommunication> iter = satellites.iterator(); iter.hasNext();){
                DataCommunication data = iter.next();
                switch(data.getName()){
                    case "kenobi": 
                        aData = new DataCommunication(data.getName(),data.getDistance(),data.getMessage());
                        break;
                    case "skywalker": 
                        bData = new DataCommunication(data.getName(),data.getDistance(),data.getMessage());
                        break;
                    case "sato": 
                        cData = new DataCommunication(data.getName(),data.getDistance(),data.getMessage());
                        break;
                    default: //satelite no reconocido
                        return new ResponseEntity<Object>(new ResponseError(404, ResponseError.SATELLITE_NOT_RECOGNIZED), HttpStatus.NOT_FOUND);
                }
            }
            if(aData == null || bData == null || cData == null) // Llego informacion de algun satelite no reconocido
                return new ResponseEntity<Object>(new ResponseError(404, ResponseError.MISSING_INVALID_SATELLITE), HttpStatus.NOT_FOUND);

            position = topSecretService.GetLocation(aData.getDistance(),bData.getDistance(),cData.getDistance());
            message = topSecretService.GetMessage(aData.getMessage(),bData.getMessage(),cData.getMessage());

            if(message != null && position != null){
                JSONObject res = new JSONObject();
                res.put("position", position);
                res.put("message", message);
                return new ResponseEntity<Object>(res, HttpStatus.OK);  
            }
            else{
                return new ResponseEntity<Object>(new ResponseError(404, ResponseError.INDETERMINATE_POSITION_MESSAGE), HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<Object>(new ResponseError(404, ResponseError.MISSING_INVALID_SATELLITE), HttpStatus.NOT_FOUND);
        }
    }
}