package com.main.desafio.controllers;


import com.main.desafio.entities.DataCommunication;
import com.main.desafio.services.DataCommunicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping ("/data_communication")
public class DataCommunicationController {
    HttpHeaders headers = new HttpHeaders();

    @Autowired
    DataCommunicationService dataCommunicationService;

    /**
     * GET /data_communication
     * Permite recuperar la infomacion almacenada de todos los satelites
     * RESPONSE BODY: JSON con una lista de objetos DataCommunication
     * RESPONSE CODE: 200 (Exito)
     */
    @GetMapping()
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<DataCommunication>> getAllDataCommunication() {
        List<DataCommunication> list = dataCommunicationService.findAll();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    /**
     * POST /data_communication
     * Permite insertar datos de comunicacion de un satelite
     * REQUEST BODY: JSON de objeto DataCommunication
     * RESPONSE CODE: 200 (Exito)
     */
    @PostMapping()
    @ResponseBody
    public ResponseEntity<Object> saveDataCommunication(@RequestBody DataCommunication data) {
        Optional<DataCommunication> optionalData = dataCommunicationService.findByName(data.getName());
        DataCommunication newData = null;
        if( optionalData.isPresent()){
            newData = new DataCommunication(optionalData.get().getName(),data.getDistance(),data.getMessage());
            newData.setId(optionalData.get().getId());
            // newData.setPx(data.getPx());
            // newData.setPy(data.getPy());
            dataCommunicationService.save(newData);
        }
        else{
            newData = dataCommunicationService.save(data);
        }
        return ResponseEntity.ok(newData);
    }

    /**
     * DELETE /data_communication
     * Permite eliminar todos los datos de comunicacion almacenados
     * RESPONSE CODE: 200 (Exito)
     */
    @DeleteMapping("")
    public ResponseEntity<?> deleteAllDataCommunication(){
        headers.setContentType(MediaType.TEXT_PLAIN);
        dataCommunicationService.deleteAll();
        return new ResponseEntity("Datos de comunicacion de los saltelites eliminados.",headers, HttpStatus.OK);
    }
}