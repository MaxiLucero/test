package com.main.desafio.repository;

import com.main.desafio.entities.DataCommunication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DataCommunicationRepository extends JpaRepository<DataCommunication, Long> {
	Optional<DataCommunication> findByName(String satelliteName);
}
